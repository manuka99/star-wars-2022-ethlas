This is a NEXT JS application designed using Material UI.
All the pages are developed to support Server Side Rendering.

## Technologies
1. Next JS
2. Next Auth (GITHUB)
3. Material UI
4. Redux

## Setting up on the local server

First, install all the required dependencies

```bash
npm install
# or
yarn
```

Then run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

