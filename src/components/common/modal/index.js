import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

const Styles = {
  ModalBox: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "80vw",
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 0,
  },
};

// CUSTOM MODAL DEVELOPED TO EASILY PLACE CONTENT INSIDE MODAL WITHOUT USING BOILER PLATED CODE
export default function CustomModal({ status, setStatus, children }) {
  const handleClose = () => setStatus(false);
  return (
    <div>
      <Modal open={status} onClose={handleClose}>
        <Box sx={Styles.ModalBox}>{children}</Box>
      </Modal>
    </div>
  );
}
