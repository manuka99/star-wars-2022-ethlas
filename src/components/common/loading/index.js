import CircularProgress from "@mui/material/CircularProgress";

const Style = {
  Main: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  CircularProgress: {
    margin: "40px auto",
  },
};
// A LODING SPINNNER (CIRCULAR)
export default function Loading() {
  const SpinnerSize = 124;
  return (
    <div style={Style.Main}>
      <CircularProgress
        sx={Style.CircularProgress}
        color="primary"
        size={SpinnerSize}
      />
    </div>
  );
}
