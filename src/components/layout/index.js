import Header from "./header";
import Footer from "./footer";
import Container from "@mui/material/Container";

const Styles = {
  Container: {
    marginTop: "32px",
    minHeight: "63vh"
  },
};

export default function Layout({ children }) {
  const Title = "STAR WARS - ETHLAS";
  const Description = "Star wars 2022 - Star Ships and Films";
  const Website = "https://ethlas.com/";
  const Year = 2022;
  return (
    <>
      <Header Title={Title} />
      <Container sx={Styles.Container}>{children}</Container>
      <Footer
        Title={Title}
        Description={Description}
        Website={Website}
        Year={Year}
      />
    </>
  );
}
