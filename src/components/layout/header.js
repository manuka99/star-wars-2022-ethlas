import * as React from "react";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import AppBar from "@mui/material/AppBar";
import Link from "next/link";
import { signIn, signOut } from "next-auth/react";
import { useSession } from "next-auth/react";

const Styles = {
  AppBar: {
    bgcolor: "black",
    height: "80px",
    display: "flex",
    justifyContent: "center",
    alignItems: "space-between",
  },
  Title: { flex: 1, cursor: "pointer", fontSize: "3vw", fontWeight: "bold", color: "yellow" },
};

function Header({ Title }) {
  const { data: session, status } = useSession();

  const handleSignIn = () => signIn("github");
  const handleSignOut = () => signOut();

  return (
    <React.Fragment>
      <AppBar sx={Styles.AppBar} position="sticky">
        <Toolbar>
          <Button variant="contained" color="warning" size="small">
            Subscribe
          </Button>
          <Link href={"/"}>
            <Typography
              align="center"
              noWrap
              sx={Styles.Title}
            >
              {Title}
            </Typography>
          </Link>
          {status == "authenticated" ? (
            <Button
              variant="contained"
              color="primary"
              size="small"
              onClick={handleSignOut}
            >
              Sign Out ({session.user.name})
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              size="small"
              onClick={handleSignIn}
            >
              Sign In (GitHub)
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

export default Header;
