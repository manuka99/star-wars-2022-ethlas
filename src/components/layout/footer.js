import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

function Copyright({ Website, Year }) {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href={Website}>
        {Website}
      </Link>{" "}
      {Year}
      {"."}
    </Typography>
  );
}

const Styles = {
  FooterBox: { bgcolor: "background.paper", py: 6 },
  FooterTitle: {
    fontSize: "18px",
    fontWeight: "bold",
    padding: "0",
    marginBottom: "0px",
  },
};

function Footer({ Title, Description, Website, Year }) {
  return (
    <Box component="footer" sx={Styles.FooterBox}>
      <Container maxWidth="lg">
        <Typography
          variant="body2"
          align="center"
          gutterBottom
          sx={Styles.FooterTitle}
        >
          {Title}
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          {Description}
        </Typography>
        <Copyright Website={Website} Year={Year} />
      </Container>
    </Box>
  );
}

Footer.propTypes = {
  Title: PropTypes.string.isRequired,
  Description: PropTypes.string.isRequired,
};

export default Footer;
