import StarShipCard from "@components/starship/mainCard";
import Grid from "@mui/material/Grid";

const Styles = {
  Main: {
    margin: "32px 0",
  },
};

// WRAPER FUNCTION FOR ALL THE STAR SHIPS
export default function StarShips({ ships, setShipModalStatus }) {
  return ships.length > 0 ? (
    <div style={Styles.Main}>
      <Grid container spacing={2}>
        {ships.map((ship) => (
          <StarShipCard
            key={ship.name}
            ship={ship}
            setShipModalStatus={setShipModalStatus}
          />
        ))}
      </Grid>
    </div>
  ) : (
    <h1>No ships found</h1>
  );
}
