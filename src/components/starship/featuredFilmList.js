import Link from "next/link";

const Styles = {
  ListUL: {
    paddingInlineStart: "14px",
    marginBlockStart: 0,
    marginBlockEnd: 0,
  },
  T15Light: {
    fontSize: "0.8rem",
    fontWeight: "light",
    color: "yellow",
  },
};

// LIST THE FILMS OF A STAR SHIP
export default function FeaturedFilmList({ films }) {
  return (
    <>
      <ul style={Styles.ListUL}>
        {films.map((film) => (
          <li key={film.url} style={Styles.T15Light}>
            <Link href={`/${film.url}`}>
              <a style={Styles.T15Light}>{film.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
}
