import Typography from "@mui/material/Typography";

// A SIMPLE ERROR HANDLER FOR STAR SHIP MAIN CARD
export default function ErrorStarShip({ mesage }) {
  return (
    <>
      <Typography
        component="h1"
        variant="h4"
        color="secondary"
        align="center"
        noWrap
      >
        {{ mesage }}
      </Typography>
    </>
  );
}
