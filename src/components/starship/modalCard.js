import Typography from "@mui/material/Typography";
import CustomModal from "@components/common/modal";
import { formatNumber } from "src/util";
import { UpdateStarShipDetailsModal } from "@store";
import { connect } from "react-redux";
import FeaturedFilmList from "./featuredFilmList";
import { Box } from "@mui/system";

const Style = {
  Main: {
    backgroundColor: "black",
    color: "yellow",
    padding: "32px",
    border: "2px yellow solid",
  },
  Title: {fontSize: "1.8rem"},
  Note: { mt: 2 },
};

// STAR SHIP ON CLICK MODAL, NOTE THE FUNCTION THAT IS USED TO UPDATED THE MODAL STATUS AND DETAILS
function StarShipModalCard({ starshipModal, UpdateStarShipModalState }) {
  const setStatus = () => UpdateStarShipModalState(false, null);
  // IF THERE IS NO DATA OR EVEN IF THE MODAL IS SET TO VISSIBLE 
  // AND SHIP INFO IS MISSING THEN IT WILL FLASH A SIMPLE ERROR MESSAGE
  if (starshipModal && starshipModal.status && !starshipModal.data) {
    alert("Invalid selection");
    setStatus(false);
  } else if (starshipModal && starshipModal.status && starshipModal.data)
    return (
      <CustomModal status={starshipModal.status} setStatus={setStatus}>
        <div style={Style.Main}>
          <Typography sx={Style.Title}>
            Star Ship Information
          </Typography>
          <Typography sx={Style.Note}>
            1. Name: {starshipModal.data.name}
          </Typography>
          <Typography sx={Style.Note}>
            2. Cost: {formatNumber(starshipModal.data.cost_in_credits)}
          </Typography>
          <Typography sx={Style.Note}>3. Featured Films:</Typography>
          <Box ml={2}>
            <FeaturedFilmList films={starshipModal.data.films} />
          </Box>
          <Typography sx={Style.Note}>
            4. Length: {starshipModal.data.length}
          </Typography>
          <Typography sx={Style.Note}>
            5. Max Atmospheric Speed:{" "}
            {starshipModal.data.max_atmosphering_speed}
          </Typography>
          <Typography sx={Style.Note}>
            6. Crew: {starshipModal.data.crew}
          </Typography>
        </div>
      </CustomModal>
    );
}

// MAP REDUX STATE AND SEND IT TO THE FUNCTIONAL COMPONENT
const mapStateToProps = (state) => {
  return {
    starshipModal: state.modals.starshipModal,
  };
};

// MAP REDUX ACTIOSN AND SEND IT TO THE FUNCTIONAL COMPONENT
const mapDispatchToProps = (dispatch) => {
  return {
    UpdateStarShipModalState: (status, data) =>
      dispatch(UpdateStarShipDetailsModal(status, data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StarShipModalCard);
