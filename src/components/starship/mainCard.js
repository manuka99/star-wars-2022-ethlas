import { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { formatNumber } from "src/util";
import Grid from "@mui/material/Grid";
import { useDispatch } from "react-redux";
import { UpdateStarShipDetailsModal } from "@store";
import FeaturedFilmList from "./featuredFilmList";
import CardActions from "@mui/material/CardActions";
import Rating from "@mui/material/Rating";
import StarIcon from "@mui/icons-material/Star";
import { useSession } from "next-auth/react";
import Swal from "sweetalert2/dist/sweetalert2.js";
import {
  GetFromLocalStorage,
  UpdateJSONLocalStorage,
} from "@services/localstorage";
import ErrorStarShip from "./error";

const Styles = {
  Card: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
    backgroundColor: "black",
    cursor: "pointer",
    transition: "transform 0.6s",
    transformStyle: "preserve-3d",
    "&:hover": { transform: "scale(1.1)" },
    p: 2,
  },
  T15Bold: { fontSize: "0.9rem", fontWeight: "bold", color: "yellow" },
  T15Light: {
    fontSize: "0.8rem",
    fontWeight: "light",
    color: "yellow",
  },
};

export default function StarShipCard({ ship }) {
  const dispatch = useDispatch();
  const [ratings, setRatings] = useState(null);
  const { data: session, status } = useSession();

  useEffect(() => {
    // if user is logged then maintain ratings
    if (
      status == "authenticated" &&
      session &&
      session.user &&
      session.user.email
    ) {
      let ratingsFromUser = GetFromLocalStorage(session.user.email, true);
      setRatings(ratingsFromUser[ship.url] ?? 0);
    }
  }, []);

  // UPDATE MODAL STATUS TO MAKE IT VISIBLE
  const handleModal = () => {
    dispatch(UpdateStarShipDetailsModal(true, ship));
  };

  // VALIDATE USER CAN ONLY ADD A RATING IF AUTHENTICATED AND SESSIONS HAS A EMAIL
  const onChangeRatings = (_event, newValue) => {
    if (status != "authenticated")
      return Swal.fire({
        title: "Error: Sign In Required!",
        text: "Please Log In to your GitHub account to add ratings.",
        icon: "error",
        confirmButtonText: "I Understand",
      });
    else if (!session || !session.user || !session.user.email)
      return Swal.fire({
        title: "Error: User Account Permissions Are Required!",
        text: "Sign In using your GitHub account and grant permission for the user profile.",
        icon: "error",
        confirmButtonText: "I Understand",
      });
    // if user is logged in maintain ratings
    setRatings(newValue);
    UpdateJSONLocalStorage(session.user.email, { [ship.url]: newValue });
  };

  if (!ship) return <ErrorStarShip mesage={"Ship details are invalid"} />;

  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <Card sx={Styles.Card} variant="elevation" elevation={4}>
        <CardContent>
          <div onClick={handleModal}>
            <Typography sx={Styles.T15Bold} gutterBottom>
              Name: {ship.name}
            </Typography>
            <Typography sx={Styles.T15Bold} gutterBottom>
              Cost: {formatNumber(ship.cost_in_credits)}
            </Typography>
            <Typography sx={Styles.T15Bold} gutterBottom>
              Featured Films:
            </Typography>
          </div>
          <FeaturedFilmList films={ship.films} />
        </CardContent>
        <CardActions>
          <Rating
            value={ratings}
            precision={0.5}
            onChange={onChangeRatings}
            emptyIcon={
              <StarIcon
                style={{ opacity: 0.55, color: "white" }}
                fontSize="inherit"
              />
            }
          />
        </CardActions>
      </Card>
    </Grid>
  );
}
