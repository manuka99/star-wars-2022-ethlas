import Typography from "@mui/material/Typography";

// A SIMPLE ERROR HANDLER FOR FILM DETAILS
export default function ErrorFilmDetails({ message }) {
  return (
    <>
      <Typography
        component="h1"
        variant="h4"
        color="secondary"
        align="center"
        noWrap
      >
        {message}
      </Typography>
    </>
  );
}
