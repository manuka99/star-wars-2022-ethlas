import Typography from "@mui/material/Typography";
import { dateToDateMonthYear } from "src/util";

export default function FilmDetailsCard({ film }) {
  return (
    <>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Title: {film.title}
      </Typography>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Episode ID: {film.episode_id}
      </Typography>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Opening Crawl: {film.opening_crawl}
      </Typography>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Director: {film.director}
      </Typography>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Producer: {film.producer}
      </Typography>
      <Typography variant="h6" color="secondary" align="left" noWrap>
        Release Date: {dateToDateMonthYear(film.release_date)}
      </Typography>
    </>
  );
}
