import { UPDATE_STARSHIP_DETAILS_MODAL } from "./modalActionTypes";

const initialState = {
  starshipModal: {
    status: false,
    data: null,
  },
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_STARSHIP_DETAILS_MODAL:
      return {
        ...state,
        starshipModal: action.payload,
      };
    default:
      return state;
  }
};
