import { UPDATE_STARSHIP_DETAILS_MODAL } from "./modalActionTypes";

// UPDATE THE START SHIP DETAILS MODAL STATUS AND THE SHIP DETAILS 
// SIMPLY THIS IS USED TO DISPLAY THE MODAL
export const UpdateStarShipDetailsModal = (status, data ) => {
  return {
    type: UPDATE_STARSHIP_DETAILS_MODAL,
    payload: { status, data },
    info: "Update status and details of the star ship modal (show or hide modal)",
  };
};