import http from "@services/http";
import FilmAPI from "@services/api/film.service";

// GET ALL STAR SHIPS
async function GetShipsAPI(params) {
  const { data, error } = await http.Request("GET", `/starships`, params);
  if (error) return { error };
  else if (data && data.results) {
    // IF STAR SHIPS ARE FOUND THEN FOR EACH STAR SHIP THEIR FILMS NEEDS TO BE FETCHED.
    for (let index = 0; index < data.results.length; index++) {
      var ship = data.results[index];
      const films = [];
      for (let index = 0; index < ship.films.length; index++) {
        const filmUrl = ship.films[index];
        // FILM IS SENT AS A URL THEREFORE IN ORDER TO GET THE ID (WHICH IS AT THE LAST), STRING IS SPLITED AND FILLTERED.
        // ONCE FILTERED EMPTY VALUES ARE REMOVED, THEN THE LAST ITEM IS THE ID
        const filmID = filmUrl
          .split("/")
          .filter((n) => n)
          .slice(-1);
        //FETCH THE FILM BY ID
        const filmResponse = await FilmAPI.GetFilmAPI(filmID);
        var film = { id: filmID };
        // IF THE FILM EXSIST THEN SAVE ITS NAME AND GENERATE A URL FROM ITS NAME WITHOUT SPACES
        if (filmResponse.data) {
          film.title = filmResponse.data.title;
          film.url = filmResponse.data.title.toString().replace(/ /g, "-");
        }
        films.push(film);
      }
      ship.films = films;
    }
    return { data };
  } else return { data };
}

// GET A SHIP BY ID
function GetShipAPI(id) {
  return http.Request("GET", `/starships/${id}`);
}

export default {
  GetShipsAPI,
  GetShipAPI,
};
