import http from "@services/http";

// GET ALL FILMS
async function GetFilmsAPI(params) {
  return await http.Request("GET", `/films`, params);
}

// GET A FILM BY ITS ID
function GetFilmAPI(id) {
  return http.Request("GET", `/films/${id}`);
}

export default {
  GetFilmsAPI,
  GetFilmAPI,
};
