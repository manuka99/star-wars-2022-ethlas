// SAVE A ITEM IN THE LOCAL STORAGE
function SaveToLocalStorage(key, value) {
  localStorage.setItem(key, value);
}

// GET A ITEM FROM LOCAL STORAGE
// IF IS JSON IS TRUE THEN THE DATA WILL BE PARSED AND RETURNED AS JSON
function GetFromLocalStorage(key, isJson = false) {
  var item = localStorage.getItem(key);
  if (isJson) {
    try {
      let parsedData = JSON.parse(item);
      return parsedData;
    } catch (error) {
      return null;
    }
  }
  return item;
}

// REMOVE DATA FROM LOCAKL STORAGE
function RemoveFromLocalStorage(key) {
  localStorage.removeItem(key);
}

// UPDATE A SAVED JSON TYPE IN LOCAL STORAGE
function UpdateJSONLocalStorage(key, newValue, retry = 0) {
  try {
    // MAX RETRIES EXCEEDED
    if (retry > 1) return;
    let item = GetFromLocalStorage(key, true);
    if (item) {
      // JOIN OLD AND NEW DATA
      let updatedData = { ...item, ...newValue };
      let stringifiedData = JSON.stringify(updatedData);
      SaveToLocalStorage(key, stringifiedData);
    } else {
      let stringifiedData = JSON.stringify(newValue);
      SaveToLocalStorage(key, stringifiedData);
    }
  } catch (e) {
    // IF ERROR REMOVE CURRUPTED DATA AND TRY AGAIN
    RemoveFromLocalStorage(key);
    UpdateJSONLocalStorage(key, newValue, ++retry);
  }
}

export { SaveToLocalStorage, GetFromLocalStorage, UpdateJSONLocalStorage };
