import config from "@config";
export default {
  Request: async (methodType, path, params, payload) => {
    return new Promise((resolve) => {
      // SENT PARAMETERS
      params = params && JSON.parse(JSON.stringify(params));
      const query = params ? `?${new URLSearchParams(params).toString()}` : "";
      const URL = `${config.API}${path}${query}`;
      //NOTE REJECT IS NOT USED, THIS IS TO SAVE TIME WHEN DEVELOPING AS IT IS EASY AND ORGANIZED TO EXTRACT ERRORS 
      fetch(URL, {
        body: JSON.stringify(payload),
        cache: "no-cache",
        headers: {
          "content-type": "application/json",
        },
        method: `${methodType}`,
      })
        .then(async (response) => {
          const data = await response.json();
          if (response.status === 200) return resolve({ data });

          return resolve({ error: data });
        })
        .catch((error) => resolve({ error: error }));
    });
  },
};
