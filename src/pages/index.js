import Head from "next/head";
import StarShipAPI from "@services/api/starship.service";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Pagination from "@mui/material/Pagination";
import Layout from "@components/layout";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import StarShips from "@components/starships";
import StarShipModalCard from "@components/starship/modalCard";
import Loading from "@components/common/loading";
import { getSession } from "next-auth/react";
import ErrorStarShips from "@components/starships/error";

const Styles = {
  Paper: {
    p: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 400,
    margin: "0 auto",
    border: "2px yellow solid",
  },
  InputBase: { ml: 1, flex: 1 },
  IconButton: { p: "10px" },
  Pagination: {
    "& .MuiPagination-ul": { display: "flex", justifyContent: "center" },
  },
};

export default function Home({ response, params }) {
  const [loading, setLoading] = useState(false);
  const [errorMSG, setErrorMSG] = useState("");
  const [ships, setShips] = useState([]);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0); //STAR SHIP RESULT
  const [searchEvent, setSearchEvent] = useState(null); // STORE ON SEARCH EVENT
  const router = useRouter();

  // constants
  const SEARCH_EVENT_TIMEOUT = 2000; //ms
  const SEARCH_INPUT_PLACEHOLDER = "Search ships by Name and Model...";

  // on the first run handle server side rendered data
  useEffect(() => {
    const { page, search } = params;
    setPage(parseInt(page));
    setSearch(search ?? "");
    ShipsDataHandler(response, params);
  }, []);

  // once user input data a timeout event will be created to fetch information and clears it on the next input if an only if the timeout time is not passed
  const onSearchShips = async ({ target: { value } }) => {
    setSearch(value ?? "");
    if (searchEvent) clearTimeout(searchEvent);
    let ev = setTimeout(() => {
      searchShipsFn(value);
    }, SEARCH_EVENT_TIMEOUT);
    setSearchEvent(ev);
  };

  // after the time is passed this functioned is triggered with the previously entered data
  // the respective api will be requested for data and the UI will be updated
  const searchShipsFn = async (search_value) => {
    setPage(1);
    const params = { page: 1 };
    if (search_value && search_value != "") params.search = search_value;
    const response = await GetShipsFn(params);
    ShipsDataHandler(response, params);
  };

  // get all the ships based on the filter parameters such as search and page
  const GetShipsFn = async (params = { search, page }) => {
    setErrorMSG(null);
    setShips([]);
    setLoading(true);
    // // Fetch data from external API
    const response = await StarShipAPI.GetShipsAPI(params);
    setLoading(false);
    return response;
  };

  // Handle ships data response
  const ShipsDataHandler = (response, params) => {
    const { data, error } = response;
    // handle error
    if (error) setErrorMSG(error.message);
    else {
      // set data and change url
      setTotalCount(data.count);
      setShips(data.results);
      var URL = params ? `?${new URLSearchParams(params).toString()}` : "";
      router.push(URL, undefined, { shallow: true });
    }
  };

  // fn triggers once the user clicks on a paginated button
  const onChangePageNo = async (event, pageNo) => {
    setPage(parseInt(pageNo));
    var params = { page: pageNo };
    if (search && search != "") params.search = search;
    const response = await GetShipsFn(params);
    ShipsDataHandler(response, params);
  };

  return (
    <Layout>
      {/* BEGIN WEBSITE HEAD METADATA */}
      <Head>
        <title>STAR WARS 2022</title>
        <meta
          name="description"
          content="STAR WARS 2022 - Star Ships & Films"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* END WEBSITE HEAD METADATA */}

      {/* BEGIN SEARCH */}
      <Paper component="form" sx={Styles.Paper}>
        <InputBase
          sx={Styles.InputBase}
          placeholder={SEARCH_INPUT_PLACEHOLDER}
          value={search}
          onChange={onSearchShips}
        />
        <IconButton type="submit" sx={Styles.IconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper>
      {/* END SEARCH */}

      {/* MAIN BOARD / STAR SHIPS */}
      {/* ONCE LOADING IS COMPLETED ERROR MESSAGE OR THE DATA WILL BE DISPLAYED */}
      {loading ? (
        <Loading />
      ) : errorMSG ? (
        <ErrorStarShips mesage={errorMSG} />
      ) : (
        <>
          <StarShips ships={ships} />
          <Pagination
            sx={Styles.Pagination}
            count={totalCount}
            page={page}
            size="large"
            onChange={onChangePageNo}
            color="primary"
            variant="outlined"
          />
          <StarShipModalCard />
        </>
      )}
    </Layout>
  );
}

// This gets called on every request
export async function getServerSideProps(ctx) {
  const { page, search } = ctx.query;
  var params = page || search ? { page, search } : { page: 1 };
  params = JSON.parse(JSON.stringify(params));
  // // Fetch data from external API
  const response = await StarShipAPI.GetShipsAPI(params);
  // Pass data to the page via props
  return { props: { response, params, session: await getSession(ctx) } };
}
