import NextAuth from "next-auth";
import GitHubProvider from "next-auth/providers/github";
import config from "@config";

// NEXT AUTH SETUP TO SUPPORT GITHUB AUTHENTICATION
export default NextAuth({
  providers: [
    GitHubProvider({
      clientId: config.GITHUB_CLIENT_ID,
      clientSecret: config.GITHUB_CLIENT_SECRET,
    }),
  ],
  callbacks: {},
  secret: config.JWT_SECRET,
});
