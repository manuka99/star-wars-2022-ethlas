import * as React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";
import theme from "@src/theme";
import createEmotionCache from "@src/theme/createEmotionCache";
import { wrapper, store } from "src/store/store";
import { Provider } from "react-redux";
import { SessionProvider } from "next-auth/react";
import "sweetalert2/src/sweetalert2.scss";
import NextNProgress from "nextjs-progressbar";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  // SESSION PROVIDER FROM THE NEXT AUTH IS ADDED TO CACHE AND GET THE SESSION DETAILS WITHOUT REQUESTING EVERYTIME
  // REDUX STORE IS ADDED
  // MUI PROVIDERS ARE ADDED
  // MUI THEME PROVIDERS ARE ADDED
  return (
    <SessionProvider session={pageProps.session}>
      <Provider store={store}>
        <CacheProvider value={emotionCache}>
          <Head>
            <meta
              name="viewport"
              content="initial-scale=1, width=device-width"
            />
          </Head>
          <ThemeProvider theme={theme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            {/* show progress on page transition */}
            <NextNProgress height={8} />
            <Component {...pageProps} />
          </ThemeProvider>
        </CacheProvider>
      </Provider>
    </SessionProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object.isRequired,
};

export default wrapper.withRedux(MyApp);
