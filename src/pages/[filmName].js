import FilmAPI from "@services/api/film.service";
import Layout from "@components/layout";
import ErrorFilmDetails from "@components/film/errorFilmDetails";
import FilmDetailsCard from "@components/film/filmDetailsCard";

// DISPLAY FILM DETAILS WITH ERROR HANDELING
export default function FilmDetails({ response, search }) {
  return (
    <Layout>
      <h2>Film Details - {search}</h2>
      {response.error ? (
        <ErrorFilmDetails message="An error occured while searching for the film. Please try again shortly!" />
      ) : response.data &&
        response.data.results &&
        response.data.results.length > 0 ? (
        <FilmDetailsCard film={response.data.results[0]} />
      ) : (
        <ErrorFilmDetails message="Requested film was not found!" />
      )}
    </Layout>
  );
}

// This gets called on every request
export async function getServerSideProps({ query }) {
  const { filmName } = query;
  const search = filmName.replace(/-/g, " ");
  const params = { search };
  // // Fetch data from external API
  const response = await FilmAPI.GetFilmsAPI(params);
  // Pass data to the page via props
  return { props: { response, search } };
}
