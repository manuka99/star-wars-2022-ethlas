// FORMAT NUMBER WITH COMMAS (USER FRIENDLY)
export function formatNumber(val) {
  try {
    return parseFloat(val).toLocaleString();
  } catch (error) {
    return "N/A";
  }
}
